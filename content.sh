#!/bin/bash
ROOT=$1
WWWROOT=$2
WWWFOLDER=$3
echo '<?xml version="1.0" encoding="UTF-8"?>'
echo '<?xml-stylesheet type="text/xsl" href="http://ottr.gitlab.io/library/templates-listing.xsl"?>'
printf '<tree root="%s" name="%s">\n' "$WWWROOT" "$WWWFOLDER"
tree -X $ROOT | tail -n +3
